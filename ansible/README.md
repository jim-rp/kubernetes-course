## Works for me! I've got:
# 3 Ubuntui 20 server VMs, non root user jim, same password, registered in DNS (using fpSense as a gateway here)

## run bash REAMDE.md - does the following:
# SetupTargets:
# - sets up ansible user on all hosts with ssh key set
# InstallKubernetes:
# - prereqs - disables swap, sets modules+sysctl params
# - installs containerd
# - installs kube components
# startCluster:
# - bootstraps cluster on control plane
# - reads join line off control plane, runs it on all workers

rm ~/.ansible/* -rf

apt install ansible sshpass


## Deb 10/11:
apt install ansible sshpass
ANSIBLE_HOST_KEY_CHECKING=false  ansible-playbook -i ./k8s.yml -u jim -k -b -K ./setUpTargets-debian.yml

## Ubuntu:
ANSIBLE_HOST_KEY_CHECKING=false  ansible-playbook -i ./k8s.yml -u jim -k -b -K ./setUpTargets-ubuntu.yml

## Azure Debian:
ANSIBLE_HOST_KEY_CHECKING=false  ansible-playbook -i ./k8s.yml -u azureuser -b -K ./setUpTargets-ubuntu.yml --key-file ~/.ssh/kube


ansible-playbook -i ./k8s.yml installKubernetes.yml

ansible-playbook -i ./k8s.yml startCluster.yml

## gluster:
ansible-playbook -i ./k8s.yml installGluster.yml
